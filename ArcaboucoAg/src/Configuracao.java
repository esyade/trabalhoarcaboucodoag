
public class Configuracao {

    private int tamanhoPopulacao;
    private double probabilidadeMutacao;
    private boolean representacao;
    private double probabilidadeCruzamento;
    private int tamanhoIndividuo ;
    private double variancia;
    private double tamanhoSelecao;

    public Configuracao(int tp, double pm, boolean r, double pc, int ti,double variancia, double tamanhoSelecao) {
        // TODO Auto-generated constructor stub
        this.tamanhoPopulacao = tp;
        this.probabilidadeMutacao = pm;
        this.representacao = r;
        this.probabilidadeCruzamento = pc;
        this.tamanhoIndividuo = ti ;
        this.variancia = variancia ;
        this.tamanhoSelecao  = tamanhoSelecao ;
    }

    public double getVariancia() {
        return variancia;
    }

    public void setVariancia(double variancia) {
        this.variancia = variancia;
    }
    

    public int getTamanhoIndividuo() {
        return tamanhoIndividuo;
    }

    public void setTamanhoIndividuo(int tamanhoIndividuo) {
        this.tamanhoIndividuo = tamanhoIndividuo;
    }
    

    public void setTamanhoPopulacao(int quantidade) {
        this.tamanhoPopulacao = quantidade;
    }

    public int getTamanhoPopulacao() {
        return this.tamanhoPopulacao;
    }

    public void setProbabilidadeMutacao(double probabilidadeMutacao) {
        this.probabilidadeMutacao = probabilidadeMutacao;
    }

    public double getProbabilidadeMutacao() {
        return this.probabilidadeMutacao;
    }

    public void setRepresentacao(boolean representacao) {
        this.representacao = representacao;
    }

    public boolean getRepresentacao() {
        return this.representacao;
    }

    public void setProbabilidadeCruzamento(double valorProbabilidade) {
        this.probabilidadeCruzamento = valorProbabilidade;
    }

    public double getProbabilidadeCruzamento() {
        return this.probabilidadeCruzamento;
    }

}
