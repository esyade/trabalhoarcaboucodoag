 
import java.util.ArrayList;
import java.util.List;

public class Avaliador {

    private List<Double> solucao = new ArrayList<>();
    private Individuo melhor ;

    public Individuo getMelhor() {
        return melhor;
    }

    public void setMelhor(Individuo melhor) {
        this.melhor = melhor;
    }

    public double calculaFitnessIndividuo(Individuo individuo) {
        // TODO calcula o fitness de 1 indiv�duo
        double somaDiferencas = 0.0, fitness = 0.0;
        int dentro =0;
        if (this.solucao.size() == individuo.getCromossomo().getDna().size()) {
            for (int i = 0; i < individuo.getCromossomo().getTamanho(); i++) {
                double tmais = this.solucao.get(i) + (this.solucao.get(i) * 0.05);
                double tmenos = this.solucao.get(i) - (this.solucao.get(i) * 0.05);
                    if(tmenos <= individuo.getCromossomo().getGene(i) && individuo.getCromossomo().getGene(i) <= tmais){
                        dentro += 1;
                        individuo.setQtdevaloresForaIntervalor(dentro);
                        somaDiferencas +=0 ;
                    }else{
                        somaDiferencas += Math.pow(this.solucao.get(i) - individuo.getCromossomo().getGene(i),2);
                    }
            }
                    fitness = 1/(1+somaDiferencas) ;
        }
        return fitness;
    }

    public List<Double> getSolucaoAlvo() {
        return this.solucao; //To change body of generated methods, choose Tools | Templates.
    }

    public int calculaValorDecimalIndividuo(Individuo individuo) {
        int valorDecimal = 0;
        int j = 0;
        int i = individuo.getCromossomo().getTamanho() - 1;
        for (double valor : individuo.getCromossomo().getDna()) {
            valorDecimal += individuo.getCromossomo().getGene(i) * (Math.pow(2, j));
            j++;
            i--;
        }
        return valorDecimal;
    }

    public int decodificarCromossomoIndividuo(Individuo individuo) {
        int valorDecimal = 0;
        valorDecimal = this.calculaValorDecimalIndividuo(individuo);
        return valorDecimal;
    }

    public Populacao avaliarPopulacao(Populacao populacao) {
        Populacao populacaoAvaliada = new Populacao();
        for (Individuo individuo : populacao.getPopulacao()) {
            Individuo IndividuoAvaliado = this.avaliarIndividuo(individuo);
            populacaoAvaliada.addIndividuo(IndividuoAvaliado);
        }
        return populacaoAvaliada;
    }

    private Individuo avaliarIndividuo(Individuo individuo) {
        double vd = 0, fitness = 0;
        fitness = this.calculaFitnessIndividuo(individuo);
        individuo.setFitness(fitness);
        return individuo;
    }

    public void  setSolucao(List<Double> k) {
        this.solucao = k ;
    }

    public void melhorIndividuoPorInteracao(Populacao populacao) {
        for(int i = 0 ; i < populacao.getTamanho();i++){
            if(populacao.getIndividuoPopulacao(i).getFitness() >= this.getMelhor().getFitness()){
                this.setMelhor(populacao.getIndividuoPopulacao(i));
            }
        }        
    }

}
