
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Ag {

    private Configuracao configuracao;
    private Avaliador avaliador;
    private Populacao populacao;

    public void addConfiguracao(Configuracao config) {
        this.configuracao = config;
    }

    public Configuracao getConfiguracao() {
        return this.configuracao;
    }

    public Populacao ordenaPopulacao(Populacao populacaoAvaliada) {
        // TODO Auto-generated method stub
        Collections.sort(populacaoAvaliada.getPopulacao(), new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                Individuo p1 = (Individuo) o1;
                Individuo p2 = (Individuo) o2;
                return p1.getFitness() > p2.getFitness() ? -1 : (p1.getFitness() < p2.getFitness() ? +1 : 0);
            }
        });
        return populacaoAvaliada;
    }

    public Populacao selecaoPorTorneio(Populacao populacaoAvaliada) {
        List<Individuo> ring = new ArrayList<>();
        Populacao populacaoParaCrossOver = new Populacao();
        Individuo melhorIndividuoDoRing = null;
        for (int i = 0; i < populacaoAvaliada.getTamanho(); i++) {
            ring = this.montaRing(populacaoAvaliada);
            melhorIndividuoDoRing = this.extraiMelhorIndividuoRing(ring);
            populacaoParaCrossOver.addIndividuo(melhorIndividuoDoRing);
        }

        return populacaoParaCrossOver;
    }

    public List<Individuo> recombinacaoGenica(Individuo individuoX, Individuo individuoY) {
        List<Double> dnaX = new ArrayList<>() , dnaY = new ArrayList<>();
        Individuo filhoX = null, filhoY = null;
        Cromossomo cromossomoX = new Cromossomo(dnaX, dnaX.size()), cromossomoY = new Cromossomo(dnaY, dnaY.size());
        double numeroAleatorio = Gerador.gerarValorReal();
        if (numeroAleatorio < this.configuracao.getProbabilidadeCruzamento()) {
            for (int iRecomb = 0; iRecomb < individuoX.getCromossomo().getTamanho(); iRecomb++) {
                double geneFilhoX = Gerador.formataCasasDecimais(individuoX.getCromossomo().getGene(iRecomb) + ( individuoY.getCromossomo().getGene(iRecomb) - individuoX.getCromossomo().getGene(iRecomb) ) * Gerador.gerarValorPorUniforme());
                double geneFilhoY = Gerador.formataCasasDecimais(individuoY.getCromossomo().getGene(iRecomb) + ( individuoX.getCromossomo().getGene(iRecomb) - individuoY.getCromossomo().getGene(iRecomb) ) * Gerador.gerarValorPorUniforme());
                cromossomoX.addGene(geneFilhoX);
                cromossomoY.addGene(geneFilhoY);
            }
        filhoX = new Individuo(cromossomoX) ;filhoY = new Individuo(cromossomoY);
        }else{
            filhoX = individuoX ; filhoY = individuoY;
        }
        List<Individuo> duplaFilhos = new ArrayList<>();
        duplaFilhos.add(filhoX);
        duplaFilhos.add(filhoY);
        return duplaFilhos;
    }

    public Populacao crossover(Populacao populacaoOrdenada) {
        Populacao geracao = new Populacao();
        double numeroAleatorio = 0.0;
        int i = 0;
        do{
                    
                            List<Individuo> dpFilhos = new ArrayList();
                            dpFilhos = this.recombinacaoGenica(populacaoOrdenada.getIndividuoPopulacao(i), populacaoOrdenada.getIndividuoPopulacao(i+1));
                            geracao.addIndividuo(dpFilhos.get(0));
                            geracao.addIndividuo(dpFilhos.get(1));
                            i+=2;
        }while(geracao.getTamanho() != this.configuracao.getTamanhoPopulacao());
        
        return geracao;
    }

    public Populacao mutacao(Populacao novaGeracao) {
        for (int i = 0; i < novaGeracao.getTamanho(); i++) {
            Individuo individuoComGenesMutados = this.mutaGeneIndividuo(novaGeracao.getIndividuoPopulacao(i));
            novaGeracao.setIndividuoPopulacao(i, individuoComGenesMutados);
        }
        return novaGeracao;
    }

    private List<Individuo> montaRing(Populacao populacaoAvaliada) {
        int quantidadeCasais = 2;
        int numeroIndice = 0;
        List<Individuo> listaCompetidores = new ArrayList<>();
        for (int i = 0; i < quantidadeCasais; i++) {
            numeroIndice = Gerador.gerarValorInteiro(populacaoAvaliada.getTamanho() - 1);
            listaCompetidores.add(populacaoAvaliada.getIndividuoPopulacao(numeroIndice));
        }
        return listaCompetidores;
    }

    private Individuo extraiMelhorIndividuoRing(List<Individuo> ring) {
        Individuo melhorDoRing = null;
        for (int i = 0; i < ring.size(); i++) {
            for (int j = 0; j < ring.size(); j++) {
                if (ring.get(j).getFitness() > ring.get(i).getFitness()) {
                    melhorDoRing = ring.get(j);
                } else {
                    melhorDoRing = ring.get(i);
                }
            }
        }
        return melhorDoRing;
    }

    private Individuo mutaGeneIndividuo(Individuo individuoPopulacao) {
        for (int i = 0; i < individuoPopulacao.getCromossomo().getTamanho(); i++) {
            double k = Gerador.gerarValorReal();
            if (k < this.configuracao.getProbabilidadeMutacao()) {
                double geneMutado = Gerador.formataCasasDecimais(Gerador.gerarValorGaussiana(individuoPopulacao.getCromossomo().getGene(i), this.configuracao.getVariancia()));
                individuoPopulacao.getCromossomo().setGene(i, geneMutado);
            }
        }
        return individuoPopulacao;
    }

    public double calculaPorIteracao(Populacao populacaoMelhores) {
         double soma = 0.0;
         double media = 0;
            for(int j = 0 ; j < populacaoMelhores.getTamanho(); j++){
                soma += populacaoMelhores.getIndividuoPopulacao(j).getFitness() ;
            }
         media = (soma / 5) ;
         return media;
    }

    
    
    

   

}
