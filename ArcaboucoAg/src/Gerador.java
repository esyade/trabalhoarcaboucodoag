
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.jfree.data.category.DefaultCategoryDataset;

public class Gerador {

    static MersenneTwister mersenneTwister = new MersenneTwister();

    private static int gerarValorBooleano() {
        // TODO gera um valor 0 ou 1 
        int valor = 0;
        if (mersenneTwister.nextBoolean() == true) {
            valor = 1;
        } else {
            valor = 0;
        }
        return valor;
    }
    
    public static double gerarValorReal() {
        double valor =  formataCasasDecimais(mersenneTwister.nextDouble());
     
        return formataCasasDecimais(valor);
    }
    
     public static int gerarValorInteiro(int limite) {
        // TODO 
        int valor = mersenneTwister.nextInt(limite);
        return  valor ;
    }

    public static List<Double> gerarListaDeValores(int tamanhoLista) {
        // TODO gera uma lista configur�vel de valores
        double valor = 0.0;
        List<Double> listaValores = new ArrayList<>();
        for (int i = 0; i < tamanhoLista; i++) {
            valor = gerarValorReal();
            listaValores.add(valor);
        }

        return listaValores;
    }
    
    public static List<Double> gerarListaDeValoresReais(int tamanhoLista) {
        // TODO gera uma lista configur�vel de valores
        double valor = 0.0;
        List<Double> listaValores = new ArrayList<>();
        for (int i = 0; i < tamanhoLista; i++) {
            valor = gerarValorReal();
            listaValores.add(valor);
        }

        return listaValores;
    }

    public static String toString(List<Integer> listaValores) {
        // TODO exibe a lista de valores
        String saida = "";
        for (int i = 0; i < listaValores.size(); i++) {
            saida += listaValores.get(i) + " | ";
        }

        return saida;
    }

    public static Cromossomo gerarCromossmoAleatorio(int tamanho) {
        List<Double> dna = Gerador.gerarListaDeValores(tamanho);
        Cromossomo cromossomo = new Cromossomo(dna, dna.size());
        return cromossomo;
    }

    public static Individuo gerarIndividuoAleatorio(int tamanho) {
        // TODO gera um indiv�duo
        Cromossomo cromossomo = gerarCromossmoAleatorio(tamanho);
        Individuo novoIndividuo = new Individuo(cromossomo);
        return novoIndividuo;
    }

    public static Populacao gerarPopulacaoAleatoriaIndividuos(int tamanhoPopulacao, int tamanhoIndividuo) {
        // TODO Auto-generated method stub
        Populacao populacaoAleatoria = new Populacao();
        List<Individuo> listaIndividuos = new ArrayList<>();
        for (int i = 0; i < tamanhoPopulacao; i++) {
            Individuo individuoAleatorio = gerarIndividuoAleatorio(tamanhoIndividuo) ;
            if(individuoAleatorio != null){
                listaIndividuos.add(individuoAleatorio);
            }
        }
        populacaoAleatoria.setPopulacao(listaIndividuos);
        return populacaoAleatoria;
    }
    
    public static double formataCasasDecimais(double valor) {
        DecimalFormat df = new DecimalFormat("0.00");
        String numeroString = df.format(valor);
        double numeroDouble = Double.valueOf(numeroString.replace(",", "."));
        return numeroDouble;
    }

    public static double gerarValorPorUniforme() {
        double  valor = mersenneTwister.nextDouble() ;
       
        return valor ;
    }

    public static double gerarValorGaussiana(double media , double variancia) {
        double  valor = mersenneTwister.nextGaussian() * variancia + media ;
        if(valor > 1 || valor < 0){
            return formataCasasDecimais(normalizaValor(media,valor,variancia));
        }
        return formataCasasDecimais(valor) ;
    }
    
    public static double normalizaValor(double media, double valor, double variancia){
        double min = (media - variancia);
        double max = (media + variancia);
        double z = (valor - min) / (max - min) ;
        return z;
    }
    
    public static void geraGrafico(List<Double> medias,String CasoTeste){
        Grafico grafico = new Grafico();
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        int i = 0;
        for(double media : medias){
            try {
                    ds = grafico.configuracaoDataSerGrafico(ds, media, CasoTeste, i);
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
            i++;
        }
          
    }

}
