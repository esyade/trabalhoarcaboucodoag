
public class Individuo {

    private Cromossomo cromossomo;
    private boolean escolhidoCrossOver;
    private boolean escolhidoMutacao;
    private int QtdevaloresDentroIntervalor ;
    private double fitness;
    private int valorDecimal;

    public int getQtdevaloresDentroIntervalor() {
        return QtdevaloresDentroIntervalor;
    }

    public void setQtdevaloresForaIntervalor(int QtdevaloresDentroIntervalor) {
        this.QtdevaloresDentroIntervalor = QtdevaloresDentroIntervalor;
    }
    
    public boolean isEscolhidoMutacao() {
        return escolhidoMutacao;
    }

    public void setEscolhidoMutacao(boolean escolhidoMutacao) {
        this.escolhidoMutacao = escolhidoMutacao;
    }

    protected boolean isEscolhidoCrossOver() {
        return escolhidoCrossOver;
    }

    protected void setEscolhidoCrossOver(boolean x) {
        this.escolhidoCrossOver = x;
    }

    protected void setCromossomo(Cromossomo cromossomo) {
        this.cromossomo = cromossomo;
    }

    public Individuo(Cromossomo cromossomo) {
        // Construtor add o cromossomo no individuo
        this.cromossomo = cromossomo;
    }

    public Cromossomo getCromossomo() {
        // retorna o  cromossomo do indiv�duo
        return this.cromossomo;
    }

    public double getFitness() {
        // TODO retorna o fitness do indiv�duo
        return this.fitness;
    }

    public void setFitness(double valorFitness) {
        // TODO altera o fitness do indiv�duo
        this.fitness = valorFitness;
    }

    public void setValorDecimal(int altura) {
        //  TODO envia a altura do indivíduo
        this.valorDecimal = altura;
    }

    public int getValorDecimal() {
        // TODO retorna a altura do indivíduo
        return this.valorDecimal;
    }

}
