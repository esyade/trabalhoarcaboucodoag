
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestePopulacao {

    @Test
    public void testaddPopulacao10Individuos() {
        Populacao populacao = new Populacao();
        List<Individuo> listaIndividuos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            List<Double> dna = new ArrayList<>();
            dna.add(0.11);
            dna.add(0.2);
            dna.add(0.16);
            dna.add(0.65);
            dna.add(0.56);
            Cromossomo cromossomo = new Cromossomo(dna, dna.size());
            Individuo novoIndividuo = new Individuo(cromossomo);
            listaIndividuos.add(novoIndividuo);
        }
        populacao.setPopulacao(listaIndividuos);
        populacao.setTamanho(listaIndividuos.size());
        assertEquals(10, populacao.getTamanho());
    }

    @Test
    public void testadd10Individuos() {
        Populacao populacao = new Populacao();
        for (int i = 0; i < 10; i++) {
            List<Double> dna = new ArrayList<>();
            dna.add(0.11);
            dna.add(0.2);
            dna.add(0.16);
            dna.add(0.65);
            dna.add(0.56);
            Cromossomo cromossomo = new Cromossomo(dna, dna.size());
            Individuo novoIndividuo = new Individuo(cromossomo);
            populacao.addIndividuo(novoIndividuo);
        }
        assertEquals(10, populacao.getTamanho());
    }

    @Test
    public void testaSubstituicaoDePopulacao() {
        Populacao populacao = new Populacao();
        for (int i = 0; i < 10; i++) {
            List<Double> dna = new ArrayList<>();
            dna.add(0.11);
            dna.add(0.2);
            dna.add(0.16);
            dna.add(0.65);
            dna.add(0.56);
            Cromossomo cromossomo = new Cromossomo(dna, dna.size());
            Individuo novoIndividuo = new Individuo(cromossomo);
            populacao.addIndividuo(novoIndividuo);
        }

        List<Individuo> listaIndividuos = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            List<Double> dna = new ArrayList<>();
            dna.add(0.11);
            dna.add(0.2);
            dna.add(0.16);
            dna.add(0.65);
            dna.add(0.56);
            Cromossomo cromossomo = new Cromossomo(dna, dna.size());
            Individuo novoIndividuo = new Individuo(cromossomo);
            listaIndividuos.add(novoIndividuo);
        }
        populacao.setPopulacao(listaIndividuos);
        populacao.setTamanho(listaIndividuos.size());
        assertEquals(5, populacao.getTamanho());
    }

    @Test
    public void testGetIndividuo() {
        Populacao populacao = new Populacao();
        for (int i = 0; i < 10; i++) {
            List<Double> dna = new ArrayList<>();
            dna.add(0.11);
            dna.add(0.2);
            dna.add(0.16);
            dna.add(0.65);
            dna.add(0.56);

            Cromossomo cromossomo = new Cromossomo(dna, dna.size());
            Individuo novoIndividuo = new Individuo(cromossomo);
            populacao.addIndividuo(novoIndividuo);
        }
        assertNotNull(populacao.getIndividuoPopulacao(6));
    }

    @Test
    public void testSetIndividuoPopulacao() {
        Populacao populacao = new Populacao();
        for (int i = 0; i < 10; i++) {
            List<Double> dna = new ArrayList<>();
            dna.add(0.11);
            dna.add(0.2);
            dna.add(0.16);
            dna.add(0.65);
            dna.add(0.56);

            Cromossomo cromossomo = new Cromossomo(dna, dna.size());
            Individuo novoIndividuo = new Individuo(cromossomo);
            populacao.addIndividuo(novoIndividuo);
        }

        List<Double> dna = new ArrayList<>();
        dna.add(0.21);
        dna.add(0.22);
        dna.add(0.61);
        dna.add(0.54);
        dna.add(0.43);

        Cromossomo cromossomo = new Cromossomo(dna, dna.size());
        Individuo outroIndividuo = new Individuo(cromossomo);
        populacao.setIndividuoPopulacao(3, outroIndividuo);

        assertEquals(5, populacao.getIndividuoPopulacao(3).getCromossomo().getTamanho());
    }

}
