import static org.junit.Assert.*;

import org.junit.Test;

public class TesteConfiguracao {
	
	@Test
	public void testConstrutorConfiguracao() {
            Configuracao config = new Configuracao(50, 0.8, true, 0.6,5,0.01,50*0.02);
		assertEquals(100, config.getTamanhoPopulacao());
	}

	@Test
	public void testTamanhoPopulacao() {
            Configuracao config = new Configuracao(50, 0.8, true, 0.6,5,0.01,50*0.02);
		config.setTamanhoPopulacao(100);
		assertEquals(100, config.getTamanhoPopulacao());
	}
	
	@Test
	public void testProbabilidadeMutacao() {
            Configuracao config = new Configuracao(50, 0.8, true, 0.6,5,0.01,50*0.02);
		config.setProbabilidadeMutacao(60.0);
		assertEquals(60.0, config.getProbabilidadeMutacao(),0);
	}
	
	@Test
	public void testRepresentacao() {
            Configuracao config = new Configuracao(50, 0.8, true, 0.6,5,0.01,50*0.02);
		config.setRepresentacao(true);
		assertTrue(config.getRepresentacao());
	}
	
	@Test
	public void testProbabilidadeCruzamento() {
            Configuracao config = new Configuracao(50, 0.8, true, 0.6,5,0.01,50*0.02);
		config.setProbabilidadeCruzamento(60.5);
		assertEquals(60.5,config.getProbabilidadeCruzamento(),0);
	}

}
