
import java.io.FileNotFoundException;
import java.io.IOException;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.data.category.DefaultCategoryDataset;

import org.junit.Test;

public class TesteAg {


    
    @Test
    public void testAddConfiguracao() {
        Configuracao config = new Configuracao(50, 0.8, true, 0.6, 5, 0.01, 50 * 0.02);
        Ag ag = new Ag();
        ag.addConfiguracao(config);
        assertNotNull(ag.getConfiguracao());
    }

    @Test
    public void testOrdenarPopulacao() {
        Configuracao config = new Configuracao(50, 0.8, true, 0.6, 5, 0.01, 50 * 0.02);
        Ag ag = new Ag();
        ag.addConfiguracao(config);
        assertNotNull(ag.getConfiguracao());
        Populacao populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(config.getTamanhoPopulacao(), config.getTamanhoIndividuo());
        List<Double> k = Gerador.gerarListaDeValoresReais(config.getTamanhoIndividuo());
        Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        //populacao.setPopulacao(avaliador.avaliarPopulacao(populacao).getPopulacao());
        // System.out.print("População avaliada, mas não ordenada\n");
        //populacao.exibeIndividuos();
        //populacao.setPopulacao(ag.ordenaPopulacao(populacao).getPopulacao());
        assertEquals(50, populacao.getTamanho(), 0);
        //System.out.print("População ordenada\n");
        //populacao.exibeIndividuos();
    }

    @Test
    public void testSelecaoPorTorneio() {
        //errada
        Configuracao config = new Configuracao(50, 0.8, true, 0.6, 5, 0.01, 50 * 0.02);
        Ag ag = new Ag();
        ag.addConfiguracao(config);
        assertNotNull(ag.getConfiguracao());
        Populacao populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(config.getTamanhoPopulacao(), config.getTamanhoIndividuo());
        List<Double> k = Gerador.gerarListaDeValoresReais(populacao.getIndividuoPopulacao(0).getCromossomo().getTamanho());
        Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        populacao = avaliador.avaliarPopulacao(populacao);
        populacao = ag.selecaoPorTorneio(populacao);
        assertEquals(50, populacao.getTamanho());
        //System.out.print("Escolhidos selecionados \n");
        //populacao.exibeIndividuos();

    }
    
    
    
     @Test
     public void testCrossOver(){
         //errado
        Configuracao config = new Configuracao(50, 0.8, true, 0.6, 5, 0.01, 3);
        Ag ag = new Ag();
        ag.addConfiguracao(config);
        assertNotNull(ag.getConfiguracao());
        Populacao populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(config.getTamanhoPopulacao(), config.getTamanhoIndividuo());
        List<Double> k = Gerador.gerarListaDeValoresReais(config.getTamanhoIndividuo());
        Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        System.out.print("População \n");
        Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao);
        System.out.print("População avaliada \n");
        Populacao  populacaoComEscolhidos = ag.selecaoPorTorneio(populacaoAvaliada);
        System.out.print("Escolhidos \n");
        Populacao populacaoOrdenada = ag.ordenaPopulacao(populacaoComEscolhidos);
        System.out.print("Escolhidos e ordenados \n");
        Populacao populacaoCruzada = ag.crossover(populacaoOrdenada) ;
        System.out.print("população com indivíduos escolhidos, ordenados e novos do cruzamento \n");
        assertEquals(50,populacaoCruzada.getTamanho());
     
        
     }

     @Test
     public void testMutacao(){
         //errado
        Configuracao config = new Configuracao(50, 0.8, true, 0.6, 5, 0.01, 3);
        Ag ag = new Ag();
        ag.addConfiguracao(config);
        assertNotNull(ag.getConfiguracao());
        Populacao populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(config.getTamanhoPopulacao(), config.getTamanhoIndividuo());
        List<Double> k = Gerador.gerarListaDeValoresReais(config.getTamanhoIndividuo());
        Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao);
        Populacao  populacaoComEscolhidos = ag.selecaoPorTorneio(populacaoAvaliada);
        Populacao populacaoOrdenada = ag.ordenaPopulacao(populacaoComEscolhidos);
        Populacao populacaoCruzada = ag.crossover(populacaoOrdenada) ;
        assertEquals(50,populacaoCruzada.getTamanho());
        Populacao populacaoMutada = ag.mutacao(populacaoCruzada);
        assertEquals(50,populacaoMutada.getTamanho());
        populacaoMutada.exibeIndividuos("nova geração mutada");
     
        
     }
    
//    @Test
//    public void testMelhorPorInteracao() {
//        //errado
//        Configuracao config = new Configuracao(10, 0.8, true, 0.6, 10, 0.01, 50 * 0.02);
//        Ag ag = new Ag();
//        Avaliador avaliador = new Avaliador();
//        ag.addConfiguracao(config);
//        Populacao populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(config.getTamanhoPopulacao(), config.getTamanhoIndividuo());
//        List<Double> dnaSolucao = Gerador.gerarListaDeValoresReais(config.getTamanhoIndividuo());
//        Cromossomo cromossomoSolucao = new Cromossomo(dnaSolucao, dnaSolucao.size());
//        Individuo individuoSolucao = new Individuo(cromossomoSolucao);        
//        avaliador.setSolucao(dnaSolucao);
//        List<Individuo> melhores = new ArrayList<>();
//        System.out.println("Individuo Solução :" + individuoSolucao.getCromossomo().toStringDna());
//        int condicaoParada = 0;
//        Individuo melhor = new Individuo(Gerador.gerarCromossmoAleatorio(0));
//        melhor.setFitness(0);
//        int iteracao = 0;
//        for(int i = 0 ; i < 10 ; i++){
//            System.out.println("--------------------------------------------------------------------------------------------------------------Iteração " + iteracao);
//            Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao);
//            populacao.exibeIndividuos("ppopulação inicial");
//            Populacao  populacaoComEscolhidos = ag.selecaoPorTorneio(populacaoAvaliada);
//            Populacao populacaoOrdenada = ag.ordenaPopulacao(populacaoComEscolhidos);
//            Populacao populacaoCruzada = ag.crossover(populacaoOrdenada) ;
//            assertEquals(10,populacaoCruzada.getTamanho());
//            Populacao populacaoMutada = ag.mutacao(populacaoCruzada);
//            assertEquals(10,populacaoMutada.getTamanho());
//            Populacao populacaoMuatadaAvaliada = avaliador.avaliarPopulacao(populacaoMutada);
//            melhores.add(avaliador.melhorIndividuoPorInteracao(populacaoMuatadaAvaliada));
//            populacao = populacaoMuatadaAvaliada;
//            populacao.exibeIndividuos("ppopulação inicial");
//            iteracao++;
//        }
//        System.out.println("Indivíduo encontrado na iteração: " + iteracao);
//        System.out.println("Melhor indivíduo: " + melhor.getCromossomo().toStringDna());
//        System.out.println("Fitness: " + melhor.getFitness());
//        assertEquals(10, melhores.size());
//    }
    
    @Test
    public void testMediaPorInteracao() throws IOException {
        //errado
        Ag ag = new Ag();
        Grafico grafico = new Grafico();
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        Avaliador avaliador = new Avaliador();
            Configuracao config = new Configuracao(100, 0.6, true, 0.9, 5, 0.5, 4);
            ag.addConfiguracao(config);
            List<Double> dnaSolucao = Gerador.gerarListaDeValoresReais(config.getTamanhoIndividuo());
            Cromossomo cromossomoSolucao = new Cromossomo(dnaSolucao, dnaSolucao.size());
            Individuo individuoSolucao = new Individuo(cromossomoSolucao);        
            avaliador.setSolucao(dnaSolucao);
            Populacao melhores = new Populacao();
            System.out.println("Individuo Solução :" + individuoSolucao.getCromossomo().toStringDna());
            int condicaoParada = 0;
            Individuo melhor = new Individuo(Gerador.gerarCromossmoAleatorio(0));
            melhor.setFitness(0);
            int iteracao = 0;
            avaliador.setMelhor(melhor);
            List<List<Double>> fitnessExecucao = new ArrayList<>();
            List<Double> fitnessIteracao = new ArrayList<>();
            for(int w = 0 ; w < 5 ; w++){
                Populacao populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(config.getTamanhoPopulacao(), config.getTamanhoIndividuo());
                for(int i = 0 ; i < 1000 ; i++){
                    Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao);
                    Populacao populacaoOrdenada = ag.ordenaPopulacao(populacaoAvaliada);
                    Populacao  populacaoComEscolhidos = ag.selecaoPorTorneio(populacaoOrdenada);
                    Populacao populacaoCruzada = ag.crossover(populacaoComEscolhidos) ;
                    assertEquals(100,populacaoCruzada.getTamanho());
                    Populacao populacaoMutada = ag.mutacao(populacaoCruzada);
                    assertEquals(100,populacaoMutada.getTamanho());
                    Populacao populacaoMuatadaAvaliada = avaliador.avaliarPopulacao(populacaoMutada);
                    avaliador.melhorIndividuoPorInteracao(populacaoMuatadaAvaliada) ;  
                    melhor = avaliador.getMelhor();
                    fitnessIteracao.add(melhor.getFitness());
                    populacao = populacaoMuatadaAvaliada ;
                }
                melhores.exibeIndividuos("Melhores de cada execução");
                fitnessExecucao.add(fitnessIteracao);
            }
            for(int i = 0; i < fitnessIteracao.size(); i++){
             double soma =0 ;
             double media = 0;
             for(int j = 0 ; j < fitnessExecucao.size();j++){
                    soma += fitnessExecucao.get(j).get(i);
                    media = soma / 5 ;
                 }
                    ds = grafico.configuracaoDataSerGrafico(ds, media, "T" + config.getTamanhoIndividuo() + "N" + config.getTamanhoPopulacao(), i);          
             }
             
         try {
                grafico.mostraGrafico(ds);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
        }
   }
   

}
