
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class testeAvaliador {

    @Test
    public void testInsereUmaSolucaoAlvo() {
        List<Double> k = new ArrayList<>();
        k.add(0.21);
        k.add(0.11);
        k.add(0.12);
        k.add(0.51);
        k.add(0.74);
        k.add(0.91);
        k.add(0.01);
        Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        assertEquals(7, avaliador.getSolucaoAlvo().size());
    }

    @Test
    public void testCalculaFitnessIndividuo() {
        List<Double> dna = new ArrayList<>();
        dna.add(0.21);
        dna.add(0.12);
        dna.add(0.11);
        dna.add(0.52);
        dna.add(0.01);
        dna.add(0.50);
        dna.add(0.90);
        Cromossomo cromossomo = new Cromossomo(dna, dna.size());
        Individuo individuo = new Individuo(cromossomo);

        List<Double> k = new ArrayList<>();
        k.add(0.21);
        k.add(0.11);
        k.add(0.12);
        k.add(0.51);
        k.add(0.74);
        k.add(0.91);
        k.add(0.01);
        Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        assertEquals(2, avaliador.calculaFitnessIndividuo(individuo),0);
    }

    @Test
    public void testCalculaValorDecimalIndividuo() {
        List<Double> dna = new ArrayList<>();
        dna.add(0, 0.11);
        dna.add(1, 0.20);
        dna.add(2, 0.21);
        dna.add(3, 0.31);
        dna.add(4, 0.41);
        dna.add(5, 0.50);
        dna.add(6, 0.60);
        Cromossomo cromossomo = new Cromossomo(dna, dna.size());
        Individuo individuo = new Individuo(cromossomo);

        List<Double> k = new ArrayList<>();

           Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        assertEquals(92, avaliador.calculaValorDecimalIndividuo(individuo));
    }

    @Test
    public void testDecodificarCromossomo() {
        Populacao populacao = new Populacao();
        List<Double> dna = new ArrayList<>();
        dna.add(0, 0.11);
        dna.add(1, 0.20);
        dna.add(2, 0.31);
        dna.add(3, 0.41);
        dna.add(4, 0.51);
        dna.add(5, 0.60);
        dna.add(6, 0.70);
        Cromossomo cromossomo = new Cromossomo(dna, dna.size());
        Individuo individuo = new Individuo(cromossomo);
        populacao.addIndividuo(individuo);
               
        List<Double> k = Gerador.gerarListaDeValoresReais(7);
           Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        assertEquals(92, avaliador.decodificarCromossomoIndividuo(populacao.getIndividuoPopulacao(0)));
    }
    
    @Test
    public void testAvaliaPopulacao() {
        Populacao populacao = new Populacao();
        
         List<Double> dna1 = new ArrayList<>();
        dna1.add(0, 0.11);
        dna1.add(1, 0.20);
        dna1.add(2, 0.31);
        dna1.add(3, 0.41);
        dna1.add(4, 0.51);
        dna1.add(5, 0.60);
        dna1.add(6, 0.70);
        
        Cromossomo cromossomo1 = new Cromossomo(dna1, dna1.size());
        
        Individuo individuo1 = new Individuo(cromossomo1);
        
        populacao.addIndividuo(individuo1);
        
        List<Double> dna2 = new ArrayList<>();
        dna2.add(0, 0.16);
        dna2.add(1, 0.56);
        dna2.add(2, 0.76);
        dna2.add(3, 0.98);
        dna2.add(4, 0.34);
        dna2.add(5, 0.32);
        dna2.add(6, 0.21);
        
        Cromossomo cromossomo2 = new Cromossomo(dna2, dna2.size());
        
        Individuo individuo2 = new Individuo(cromossomo2);
        
        populacao.addIndividuo(individuo2);
        
        List<Double> dna3 = new ArrayList<>();
        dna3.add(0, 0.21);
        dna3.add(1, 0.32);
        dna3.add(2, 0.43);
        dna3.add(3, 0.54);
        dna3.add(4, 0.65);
        dna3.add(5, 0.76);
        dna3.add(6, 0.87);
        
        Cromossomo cromossomo3 = new Cromossomo(dna3, dna3.size());
        
        Individuo individuo3 = new Individuo(cromossomo3);
        
        populacao.addIndividuo(individuo3);
        
        
        List<Double> k = new ArrayList<>();
        k.add(0.21);
        k.add(0.11);
        k.add(0.12);
        k.add(0.51);
        k.add(0.74);
        k.add(0.91);
        k.add(0.01);
        
           Avaliador avaliador = new Avaliador();
        avaliador.setSolucao(k);
        
        assertEquals(3,avaliador.avaliarPopulacao(populacao).getTamanho());
        assertEquals(2,avaliador.avaliarPopulacao(populacao).getIndividuoPopulacao(0).getFitness(),0);
        assertEquals(1,avaliador.avaliarPopulacao(populacao).getIndividuoPopulacao(1).getFitness(),0);
        assertEquals(3,avaliador.avaliarPopulacao(populacao).getIndividuoPopulacao(2).getFitness(),0);
        
    }
    
    
    

}
